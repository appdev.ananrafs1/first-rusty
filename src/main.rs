use std::fmt::Debug;

fn main() {
    println!("Hello, world!");


    // OWNERSHIP
    let mut y = 32;
    let m = &mut y;
    *m += 32;
    println!("{}",m);
    //y += 32; // error karena mutable m masih nulis (code yang dibawah), rust memastikan setiap variable hanya bisa ditulis oleh yang owned, dalam hal ini m meminjam kepemilikan y, sehingga m harus mengembalikan (freed), agar y bisa menulis
    *m += 32;
    println!("{}", y); // karena m sudah tidak digunakan, y sekarang berhak memiliki 



    // REFERENCE
    let x = 45;
    let y =27;

    let refx = &x; // refeerence ke x (read only)
    let refy = &y; // refeerence ke y (read only)

    let refrefx = &refx; // reference ke refx (read only) yang reference ke x
    let refrefy = &refy; // reference ke refy (read only) yang reference ke y

    //rust secara langsung mengambil value dari base reference
    println!("{}", refrefx);
    println!("{}", **refrefx); //equal sama yang atas

    println!("{}", refrefy);
    println!("{}", **refrefy);

    //comparing y >= x (value not reference)
    println!("{}", refrefy == refrefx);  
    println!("{}", **refrefy >= **refrefx);  //equal sama yang atas
    //println!("{}", refrefy >= **refrefx);  //type harus match dalam comparation
    println!("{}", refrefy >= &refx);  //type harus match dalam comparation


    // LIFETIME
    #[derive(Debug)]
    struct Lifetimelearn<'a> {
        prop : &'a i32
    }

    let ltime : Lifetimelearn;
    {
        let shortlive = 28;
        ltime = Lifetimelearn { prop : &812} // rust akan membuat anonym var untuk menampung reference dari 812 ( &812) dengan lifetime menyesuaikan dari prop
        // ltime = Lifetimelearn {prop : &shortlive} // error karena shortlive akan di drop di line yang bawah, rust menolak karena ltime akan hidup lebih lama dari shortlive
    } // shortlive akan di freed disini, sehingga segala reference ke shortlive akan menyebabkan error ( dan rust memproteksi ini dengan lifetime)
    println!("{:?}", ltime)

}
